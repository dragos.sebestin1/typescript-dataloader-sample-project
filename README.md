## Getting started

`npm install`

Compile ts code to js (will create a `build` folder with the results)
`npm run build`

After compiliing the code, the project needs to copy graphql types to the `build` folder.
This is needed because in the code they are read from the disk using relative paths to
the root folder which is `src` for TS and `build` for JS.
`npm run copyGraphqlFiles`

Run the project in dev mode with automatic restarts using nodemon
`npm run dev`

Also useful while developing to start the Typescript compiler in watch mode
so it will pick up changes to `.ts` files and rebuild them (it uses incremental builds
that only compile the changed file so it's a lot faster).
`npm run build:watch`

### Debugging

Commited in the repo is also the VSCode `lauch.json` file with the config for the built in debugger.
The typescript compiler also generates source maps (https://www.typescriptlang.org/tsconfig#sourceMap)
so you can set breakpoints in `.ts` files.
