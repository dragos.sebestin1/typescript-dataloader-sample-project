import { Sequelize } from "sequelize";
import { BudgetRepository } from "./domain/bugets/budgetRepository";
import { CampaignRepository } from "./domain/campaigns/campaignRepository";
import { TeamRepository } from "./domain/teams/teamsRepository";

/**
 * Class used as app wrapper. Handles creating db connections,
 * initializing repositories and cleaning up upon shutdown request.
 */
export class App {
  private _sqlize?: Sequelize;

  private _campaignsRepo?: CampaignRepository;
  private _budgetsRepo?: BudgetRepository;
  private _teamsRepo?: TeamRepository;

  /**
   * Class constructor.
   */
  constructor() {}

  /**
   * Bootstrap app.
   */
  async start() {
    // init db connection
    this._sqlize = new Sequelize(
      "mysql://root@localhost:3306/dragos-campaign-service"
    );

    await this._sqlize.authenticate();

    // init repos
    this._campaignsRepo = new CampaignRepository(this._sqlize);
    this._budgetsRepo = new BudgetRepository(this._sqlize);
    this._teamsRepo = new TeamRepository(this._sqlize);

    // update DB schema (used on dev mostly)
    await this._sqlize.sync();
  }

  /**
   * Shutdown app.
   */
  async end() {
    if (this._sqlize) {
      await this._sqlize.close();
    }
  }

  // repo getters
  get campaignsRepository() {
    if (!this._campaignsRepo) throw new Error("DB connection not initialized");
    return this._campaignsRepo;
  }
  get budgetsRepository() {
    if (!this._budgetsRepo) throw new Error("DB connection not initialized");
    return this._budgetsRepo;
  }
  get teamsRepository() {
    if (!this._teamsRepo) throw new Error("DB connection not initialized");
    return this._teamsRepo;
  }
}
