import { IBudget } from "../types";

export class Budget implements IBudget {
  public id: number;
  public campaignId: number;
  public providerId: number;
  public allocatedValue: number;

  /**
   * Class constructor.
   */
  constructor(data: IBudget) {
    this.id = data.id;
    this.campaignId = data.campaignId;
    this.providerId = data.providerId;
    this.allocatedValue = data.allocatedValue;
  }

  /**
   * Create a new budget.
   */
  static createNew({
    campaignId,
    providerId,
    allocatedValue,
  }: {
    campaignId: number;
    providerId: number;
    allocatedValue: number;
  }) {
    // data validation
    if (allocatedValue < 0)
      throw new Error("Allocated value cannot be less than 0");

    return new Budget({ id: -1, campaignId, providerId, allocatedValue });
  }
}
