import { ICampaign } from "../types";
import { Budget } from "./budget";
import { BudgetRepository } from "./budgetRepository";

/**
 * Create a new budget for a campaign.
 */
export async function createNewBudget({
  budgetRepo,
  campaign,
  providerId,
  allocatedValue,
}: {
  budgetRepo: BudgetRepository;
  campaign: ICampaign;
  providerId: number;
  allocatedValue: number;
}) {
  const newBudget = Budget.createNew({
    campaignId: campaign.id,
    providerId,
    allocatedValue,
  });

  const createdBudget = await budgetRepo.createNew({ budget: newBudget });

  return createdBudget;
}
