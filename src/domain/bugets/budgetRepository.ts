import { Sequelize, DataTypes, Model, ModelStatic, Op, where } from "sequelize";
import { IBudget } from "../types";

class BudgetModel extends Model {
  declare id: number;
  declare campaignId: number;
  declare providerId: number;
  declare allocatedValue: number;
}

export class BudgetRepository {
  private _model: ModelStatic<BudgetModel>;

  /**
   * Class constructor.
   */
  constructor(connection: Sequelize) {
    this._model = BudgetModel.init(
      {
        id: {
          type: DataTypes.INTEGER,
          allowNull: false,
          primaryKey: true,
          autoIncrement: true,
        },
        campaignId: {
          type: DataTypes.INTEGER,
          allowNull: false,
          references: {
            model: "campaigns",
            key: "id",
          },
        },
        providerId: {
          type: DataTypes.INTEGER,
          allowNull: false,
        },
        allocatedValue: {
          type: DataTypes.FLOAT,
          allowNull: false,
        },
      },
      { sequelize: connection, modelName: "budgets" }
    );
  }

  /**
   * Create a new capmpaign.
   */
  async createNew({ budget }: { budget: IBudget }): Promise<IBudget> {
    const newBudgetModel = this._model.build({
      campaignId: budget.campaignId,
      providerId: budget.providerId,
      allocatedValue: budget.allocatedValue,
    });

    return newBudgetModel.save().then((res) => {
      const createdBudgetModel = res.get();

      return {
        id: createdBudgetModel.id,
        campaignId: createdBudgetModel.campaignId,
        providerId: createdBudgetModel.providerId,
        allocatedValue: createdBudgetModel.allocatedValue,
      };
    });
  }

  /**
   * Get budgets for campaigns with provided ids.
   */
  async getAllBudgetsForCampaign({
    campaignId,
  }: {
    campaignId: number;
  }): Promise<IBudget[]> {
    const rows = await this._model.findAll({
      where: { campaignId: { [Op.eq]: campaignId } },
    });

    return rows.map((row) => ({
      id: row.id,
      campaignId: row.campaignId,
      providerId: row.providerId,
      allocatedValue: row.allocatedValue,
    }));
  }
}
