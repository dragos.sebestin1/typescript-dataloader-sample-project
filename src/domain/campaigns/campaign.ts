import { IBudget, ICampaign, OptimizationType } from "../types";

/**
 * Class used to work with a campaign.
 */
export class Campaign implements ICampaign {
  public id: number;
  public team_id: number;
  public title: string;
  public description?: string;
  public start_date?: Date;
  public end_date?: Date;
  public optimization_type: OptimizationType;
  public active: boolean;

  /**
   * Class constructor.
   */
  constructor(data: ICampaign) {
    this.id = data.id;
    this.team_id = data.team_id;
    this.title = data.title;
    this.description = data.description;
    this.start_date = data.start_date;
    this.end_date = data.end_date;
    this.optimization_type = data.optimization_type;
    this.active = data.active;
  }

  /**
   * Static method used to create a new campaign instance
   * with default values where needed.
   */
  static createNew({
    team_id,
    title,
    description,
    startDate,
    endDate,
  }: {
    team_id: number;
    title: string;
    description?: string;
    startDate: Date;
    endDate: Date;
  }) {
    // perform data validation (trim strings etc)

    return new Campaign({
      id: 0,
      team_id,
      title: title.trim(),
      description: description?.trim(),
      start_date: startDate,
      end_date: endDate,
      optimization_type: OptimizationType.auto,
      active: true,
    });
  }

  /**
   * Compute total budget from budgets alocated per provider.
   */
  computeTotalBudget(providerBudgets: IBudget[]) {
    return providerBudgets.reduce(
      (sum, budget) => sum + budget.allocatedValue,
      0
    );
  }
}
