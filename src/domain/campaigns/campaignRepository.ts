import { Sequelize, DataTypes, Model, ModelStatic, Op } from "sequelize";
import { ICampaign, OptimizationType } from "../types";

class CampaignModel extends Model {
  declare id: number;
  declare team_id: number;
  declare title: string;
  declare description?: string;
  declare start_date?: Date;
  declare end_date?: Date;
  declare optimization_type: OptimizationType;
  declare active: boolean;
}

export class CampaignRepository {
  private _model: ModelStatic<CampaignModel>;

  /**
   * Class constructor.
   */
  constructor(connection: Sequelize) {
    // init model
    this._model = CampaignModel.init(
      {
        id: {
          type: DataTypes.INTEGER,
          allowNull: false,
          primaryKey: true,
          autoIncrement: true,
        },
        team_id: {
          type: DataTypes.INTEGER,
          references: {
            model: "teams",
            key: "id",
          },
        },
        title: {
          type: DataTypes.STRING,
          allowNull: false,
        },
        description: {
          type: DataTypes.TEXT,
          allowNull: true,
        },
        start_date: {
          type: DataTypes.DATE,
          allowNull: true,
        },
        end_date: {
          type: DataTypes.DATE,
          allowNull: true,
        },
        optimization_type: {
          type: DataTypes.ENUM("AUTO", "MANUAL"),
          allowNull: true,
        },
        active: {
          type: DataTypes.BOOLEAN,
          allowNull: false,
          defaultValue: true,
        },
      },
      {
        sequelize: connection,
        modelName: "campaigns",
      }
    );
  }

  /**
   * Create a new capmpaign.
   */
  async createNew({ campaign }: { campaign: ICampaign }): Promise<ICampaign> {
    const newCampaignModel = this._model.build({
      team_id: campaign.team_id,
      title: campaign.title,
      description: campaign.description,
      start_date: campaign.start_date,
      end_date: campaign.end_date,
      optimization_type: campaign.optimization_type,
      active: campaign.active,
    });

    return newCampaignModel.save().then((res) => {
      const createdCampaignModel = res.get();

      return {
        id: createdCampaignModel.id,
        team_id: createdCampaignModel.team_id,
        title: createdCampaignModel.title,
        description: createdCampaignModel.description,
        start_date: createdCampaignModel.start_date,
        end_date: createdCampaignModel.end_date,
        optimization_type: createdCampaignModel.optimization_type,
        active: createdCampaignModel.active,
      };
    });
  }

  /**
   * Get a campaign by id.
   */
  async getById(id: number): Promise<ICampaign | null> {
    const found = await this._model.findOne({
      where: {
        id,
      },
    });

    if (!found) return null;

    return {
      id: found.id,
      team_id: found.team_id,
      title: found.title,
      description: found.description,
      start_date: found.start_date,
      end_date: found.end_date,
      optimization_type: found.optimization_type,
      active: found.active,
    };
  }

  /**
   * Batch fetch multiple campaigns by id.
   */
  async findAllById({
    ids,
  }: {
    ids: readonly number[];
  }): Promise<Array<ICampaign | Error>> {
    const foundItems = await this._model.findAll({
      where: { id: { [Op.in]: ids } },
    });

    return ids.map((id) => {
      const found = foundItems.find((item) => item.id === id);
      if (!found) return new Error(`could not find campaign with id ${id}`);

      return {
        id: found.id,
        team_id: found.team_id,
        title: found.title,
        description: found.description,
        start_date: found.start_date,
        end_date: found.end_date,
        optimization_type: found.optimization_type,
        active: found.active,
      };
    });
  }

  /**
   * List campaigns.
   */
  async list({
    limit,
    after,
  }: {
    limit: number;
    after?: number;
  }): Promise<{ count: number; campaigns: ICampaign[] }> {
    const result = await this._model.findAndCountAll({
      where: {
        ...(after && { id: { [Op.gt]: after } }),
      },
      limit,
    });

    return {
      count: result.count,
      campaigns: result.rows.map((item) => ({
        id: item.id,
        team_id: item.team_id,
        title: item.title,
        description: item.description,
        start_date: item.start_date,
        end_date: item.end_date,
        optimization_type: item.optimization_type,
        active: item.active,
      })),
    };
  }
}
