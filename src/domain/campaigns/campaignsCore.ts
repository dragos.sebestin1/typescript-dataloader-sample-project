/**
 * File containing CORE methods for working with campaigns.
 */

import { BudgetRepository } from "../bugets/budgetRepository";
import { ICampaign, IConnection } from "../types";
import { Campaign } from "./campaign";
import { CampaignRepository } from "./campaignRepository";
import { CampaignDataLoader } from "./dataLoaders";

/**
 * Create a campaign.
 */
export async function createCampaign({
  campaignRepo,
  newCampaignInput,
}: {
  campaignRepo: CampaignRepository;
  newCampaignInput: {
    team_id: number;
    title: string;
    description?: string;
    startDate: Date;
    endDate: Date;
  };
}): Promise<ICampaign> {
  // instantiate new campaign with emtpy id
  const { team_id, title, description, startDate, endDate } = newCampaignInput;
  const newCampaign = Campaign.createNew({
    team_id,
    title,
    description,
    startDate,
    endDate,
  });

  // save it to DB and return the created one with id
  const createdCampaign = await campaignRepo.createNew({
    campaign: newCampaign,
  });

  return createdCampaign;
}

/**
 * Fetch a campaign by id.
 */
export async function getCampaignById({
  dataLoader,
  campaignId,
}: {
  dataLoader: CampaignDataLoader;
  campaignId: number;
}) {
  const found = await dataLoader.load(campaignId);

  return found;
}

/**
 * List team campaigns.
 */
export async function listCampaigns({
  campaignRepo,
  first = 10,
  after,
}: {
  campaignRepo: CampaignRepository;
  first?: number;
  after?: number;
}): Promise<IConnection<ICampaign>> {
  const result = await campaignRepo.list({ limit: first, after });

  const firstItem = result.campaigns[0];
  const lastItem = result.campaigns[result.campaigns.length - 1];

  return {
    edges: result.campaigns.map((c) => ({
      node: c,
      cursor: c.id.toString(),
    })),
    pageInfo: {
      endCursor: lastItem.id?.toString(),
      hasNextPage: (lastItem?.id ?? 0) <= (after ?? 0 + first),
      hasPreviousPage: !!after && after < 1,
      startCursor: firstItem.id?.toString(),
    },
  };
}

/**
 * Compute total budget for a campaign.
 */
export async function computeCampaignTotalBudget({
  budgetsRepo,
  campaignData,
}: {
  budgetsRepo: BudgetRepository;
  campaignData: ICampaign;
}) {
  // fetch all budgets for this campaign
  const allBudgets = await budgetsRepo.getAllBudgetsForCampaign({
    campaignId: campaignData.id,
  });

  const campaign = new Campaign(campaignData);

  return campaign.computeTotalBudget(allBudgets);
}
