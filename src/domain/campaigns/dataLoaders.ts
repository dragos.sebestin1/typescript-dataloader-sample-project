import DataLoader from "dataloader";
import { ICampaign } from "../types";
import { CampaignRepository } from "./campaignRepository";

export type CampaignDataLoader = DataLoader<number, ICampaign>;

export const createCampaignDataLoader = (
  campaignRepo: CampaignRepository
): CampaignDataLoader =>
  new DataLoader<number, ICampaign>((keys) =>
    campaignRepo.findAllById({ ids: keys })
  );
