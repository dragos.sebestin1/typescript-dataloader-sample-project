import DataLoader from "dataloader";
import { ITeam } from "../types";
import { TeamRepository } from "./teamsRepository";

export type TeamDataLoader = DataLoader<number, ITeam>;
export const createTeamDataLoader = (
  teamsRepo: TeamRepository
): TeamDataLoader =>
  new DataLoader<number, ITeam>((keys) => teamsRepo.findAllById({ ids: keys }));
