import { ITeam } from "../types";

export class Team implements ITeam {
  public id: number;
  public name: string;
  public description?: string;

  /**
   * Class constructor.
   */
  constructor(data: ITeam) {
    this.id = data.id;
    this.name = data.name;
    this.description = data.description;
  }

  /**
   * Create a new team.
   */
  static createNew({
    name,
    description,
  }: {
    name: string;
    description?: string;
  }) {
    return new Team({
      id: 0,
      name: name.trim(),
      description: description?.trim(),
    });
  }
}
