import { IConnection, ITeam } from "../types";
import { Team } from "./team";
import { TeamRepository } from "./teamsRepository";

/**
 * Register a new team in the app.
 */
export async function registerNewTeam({
  teamsRepo,
  name,
  description,
}: {
  teamsRepo: TeamRepository;
  name: string;
  description?: string;
}) {
  const newTeam = Team.createNew({ name, description });

  const createdTeam = await teamsRepo.createNew({ team: newTeam });

  return createdTeam;
}

/**
 * List teams.
 */
export async function listTeams({
  teamRepo,
  first = 10,
  after,
}: {
  teamRepo: TeamRepository;
  first?: number;
  after?: number;
}): Promise<IConnection<ITeam>> {
  const result = await teamRepo.list({ limit: first, after });

  const firstItem = result.teams[0];
  const lastItem = result.teams[result.teams.length - 1];

  return {
    edges: result.teams.map((c) => ({
      node: c,
      cursor: c.id.toString(),
    })),
    pageInfo: {
      endCursor: lastItem.id?.toString(),
      hasNextPage: (lastItem?.id ?? 0) < (after ?? 0 + first),
      hasPreviousPage: !!after && after < 1,
      startCursor: firstItem.id?.toString(),
    },
  };
}
