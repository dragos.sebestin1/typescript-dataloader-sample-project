import { Sequelize, DataTypes, Model, ModelStatic, Op } from "sequelize";
import { ITeam } from "../types";

class TeamModel extends Model {
  declare id: number;
  declare name: string;
  declare description?: string;
}

export class TeamRepository {
  private _model: ModelStatic<TeamModel>;

  /**
   * Class constructor.
   */
  constructor(connection: Sequelize) {
    this._model = TeamModel.init(
      {
        id: {
          type: DataTypes.INTEGER,
          allowNull: false,
          primaryKey: true,
          autoIncrement: true,
        },
        name: {
          type: DataTypes.STRING,
          allowNull: false,
        },
        description: {
          type: DataTypes.STRING(2000),
          allowNull: true,
        },
      },
      { sequelize: connection, modelName: "teams" }
    );
  }

  /**
   * Add a new team entry to the database.
   */
  async createNew({ team }: { team: ITeam }): Promise<ITeam> {
    const newTeamModel = this._model.build({
      name: team.name,
      description: team.description,
    });

    return newTeamModel.save().then((res) => {
      const createdTeamModel = res.get();

      return {
        id: createdTeamModel.id,
        name: createdTeamModel.name,
        description: createdTeamModel.description,
      };
    });
  }

  /**
   * Batch fetch multiple campaigns by id.
   */
  async findAllById({
    ids,
  }: {
    ids: readonly number[];
  }): Promise<Array<ITeam | Error>> {
    const foundItems = await this._model.findAll({
      where: { id: { [Op.in]: ids } },
    });

    return ids.map((id) => {
      const found = foundItems.find((item) => item.id === id);
      if (!found) return new Error(`could not find team with id ${id}`);

      return {
        id: found.id,
        name: found.name,
        description: found.description,
      };
    });
  }

  /**
   * List campaigns.
   */
  async list({
    limit,
    after,
  }: {
    limit: number;
    after?: number;
  }): Promise<{ count: number; teams: ITeam[] }> {
    const result = await this._model.findAndCountAll({
      where: {
        ...(after && { id: { [Op.gt]: after } }),
      },
      limit,
    });

    return {
      count: result.count,
      teams: result.rows.map((item) => ({
        id: item.id,
        name: item.name,
        description: item.description,
      })),
    };
  }
}
