export enum OptimizationType {
  auto = "AUTO",
  manual = "MANUAL",
}

/**
 * Interface used to work with campaign JSON objects.
 */
export interface ICampaign {
  id: number;
  team_id: number;
  title: string;
  description?: string;
  start_date?: Date;
  end_date?: Date;
  optimization_type: OptimizationType;
  active: boolean;
}

/**
 * Interface used to work with budget JSON objects.
 */
export interface IBudget {
  id: number;
  campaignId: number;
  providerId: number;
  allocatedValue: number;
}

/**
 * Interface used to work with Team JSON objects.
 */
export interface ITeam {
  id: number;
  name: string;
  description?: string;
}

/**
 * Pagination
 */
export interface IPageInfo {
  endCursor?: string;
  hasNextPage: boolean;
  hasPreviousPage: boolean;
  startCursor?: string;
}

export interface IEdge<T> {
  node: T;
  cursor: string;
}

export interface IConnection<T> {
  edges: IEdge<T>[];
  pageInfo: IPageInfo;
}
