import { App } from "../app";
import { BudgetRepository } from "../domain/bugets/budgetRepository";
import { CampaignRepository } from "../domain/campaigns/campaignRepository";
import {
  CampaignDataLoader,
  createCampaignDataLoader,
} from "../domain/campaigns/dataLoaders";
import {
  createTeamDataLoader,
  TeamDataLoader,
} from "../domain/teams/dataLoaders";
import { TeamRepository } from "../domain/teams/teamsRepository";

/**
 * Type used for the "context" param of
 * a GraphQL request.
 */
export interface IGraphQLContext {
  logger: any; // init logger instance with session support

  repositories: {
    campaignRepo: CampaignRepository;
    budgetsRepo: BudgetRepository;
    teamsRepo: TeamRepository;
  };
  dataLoaders: {
    campaignDataLoader: CampaignDataLoader;
    teamDataLoader: TeamDataLoader;
  };
}

export function initGqlContext(app: App): IGraphQLContext {
  const campaignRepo = app.campaignsRepository;
  const budgetsRepo = app.budgetsRepository;
  const teamsRepo = app.teamsRepository;

  return {
    logger: console,
    repositories: {
      campaignRepo,
      budgetsRepo,
      teamsRepo,
    },
    dataLoaders: {
      campaignDataLoader: createCampaignDataLoader(campaignRepo),
      teamDataLoader: createTeamDataLoader(teamsRepo),
    },
  };
}
