import { createNewBudget } from "../../../domain/bugets/budgetCore";
import { IGraphQLContext } from "../../context";

export const budgetMutations = {
  createBudget: async (
    _: any,
    args: {
      budget: {
        campaignId: number;
        providerId: number;
        allocatedValue: number;
      };
    },
    ctx: IGraphQLContext
  ) => {
    const campaign = await ctx.dataLoaders.campaignDataLoader.load(
      args.budget.campaignId
    );

    return createNewBudget({
      budgetRepo: ctx.repositories.budgetsRepo,
      campaign,
      providerId: args.budget.providerId,
      allocatedValue: args.budget.allocatedValue,
    });
  },
};
