import { createCampaign } from "../../../domain/campaigns/campaignsCore";
import { IGraphQLContext } from "../../context";

export const campaignMutations = {
  createCampaign: async (
    _: any,
    args: {
      campaign: {
        team_id: string;
        title: string;
        description?: string;
        start_date: Date;
        end_date: Date;
      };
    },
    ctx: IGraphQLContext
  ) => {
    return createCampaign({
      campaignRepo: ctx.repositories.campaignRepo,
      newCampaignInput: {
        team_id: parseInt(args.campaign.team_id),
        title: args.campaign.title,
        description: args.campaign.description,
        startDate: args.campaign.start_date,
        endDate: args.campaign.end_date,
      },
    });
  },
};
