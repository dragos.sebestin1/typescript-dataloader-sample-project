import {
  computeCampaignTotalBudget,
  getCampaignById,
  listCampaigns,
} from "../../../domain/campaigns/campaignsCore";
import { IGraphQLContext } from "../../context";

export const campaignQueries = {
  campaigns: async (
    _: any,
    args: { first?: string; after?: string },
    ctx: IGraphQLContext
  ) => {
    const result = await listCampaigns({
      campaignRepo: ctx.repositories.campaignRepo,
      first: !!args.first ? parseInt(args.first) : undefined,
      after: !!args.after ? parseInt(args.after) : undefined,
    });

    // populate the cache for field resolvers
    result.edges.map((edge) => {
      ctx.dataLoaders.campaignDataLoader.prime(edge.node.id, edge.node);
    });

    return result;
  },
  campaign: async (_: any, args: { id: string }, ctx: IGraphQLContext) => ({
    id: args.id,
  }),
};

export const campaignFieldResolvers = {
  Campaign: {
    id: async (root: { id: string }, _: any, ctx: IGraphQLContext) => {
      const campaign = await getCampaignById({
        dataLoader: ctx.dataLoaders.campaignDataLoader,
        campaignId: parseInt(root.id),
      });

      return campaign.id;
    },
    team: async (root: { id: string }, _: any, ctx: IGraphQLContext) => {
      const campaign = await getCampaignById({
        dataLoader: ctx.dataLoaders.campaignDataLoader,
        campaignId: parseInt(root.id),
      });

      const team = await ctx.dataLoaders.teamDataLoader.load(campaign.team_id);
      return team;
    },
    title: async (root: { id: string }, _: any, ctx: IGraphQLContext) => {
      const campaign = await getCampaignById({
        dataLoader: ctx.dataLoaders.campaignDataLoader,
        campaignId: parseInt(root.id),
      });

      return campaign.title;
    },
    description: async (root: { id: string }, _: any, ctx: IGraphQLContext) => {
      const campaign = await getCampaignById({
        dataLoader: ctx.dataLoaders.campaignDataLoader,
        campaignId: parseInt(root.id),
      });

      return campaign.description;
    },
    start_date: async (root: { id: string }, _: any, ctx: IGraphQLContext) => {
      const campaign = await getCampaignById({
        dataLoader: ctx.dataLoaders.campaignDataLoader,
        campaignId: parseInt(root.id),
      });

      return campaign.start_date;
    },
    end_date: async (root: { id: string }, _: any, ctx: IGraphQLContext) => {
      const campaign = await getCampaignById({
        dataLoader: ctx.dataLoaders.campaignDataLoader,
        campaignId: parseInt(root.id),
      });

      return campaign.end_date;
    },
    optimization_type: async (
      root: { id: string },
      _: any,
      ctx: IGraphQLContext
    ) => {
      const campaign = await getCampaignById({
        dataLoader: ctx.dataLoaders.campaignDataLoader,
        campaignId: parseInt(root.id),
      });

      return campaign.optimization_type;
    },
    active: async (root: { id: string }, _: any, ctx: IGraphQLContext) => {
      const campaign = await getCampaignById({
        dataLoader: ctx.dataLoaders.campaignDataLoader,
        campaignId: parseInt(root.id),
      });

      return campaign.active;
    },
    totalBudget: async (root: { id: string }, _: any, ctx: IGraphQLContext) => {
      const campaign = await getCampaignById({
        dataLoader: ctx.dataLoaders.campaignDataLoader,
        campaignId: parseInt(root.id),
      });

      return computeCampaignTotalBudget({
        budgetsRepo: ctx.repositories.budgetsRepo,
        campaignData: campaign,
      });
    },
  },
};
