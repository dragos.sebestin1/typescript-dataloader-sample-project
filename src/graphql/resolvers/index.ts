import {
  campaignMutations,
  campaignQueries,
  campaignFieldResolvers,
} from "./campaigns";
import { budgetMutations } from "./budgets";
import {
  teamQueries,
  teamFieldResolvers,
  teamEdgeResolvers,
  teamMutations,
} from "./teams";

export const resolvers = {
  Query: {
    ...campaignQueries,
    ...teamQueries,
  },
  Mutation: {
    ...campaignMutations,
    ...budgetMutations,
    ...teamMutations,
  },
  ...campaignFieldResolvers,
  ...teamFieldResolvers,
  // ...teamEdgeResolvers,
};
