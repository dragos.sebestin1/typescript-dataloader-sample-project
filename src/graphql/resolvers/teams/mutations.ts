import { registerNewTeam } from "../../../domain/teams/teamCore";
import { IGraphQLContext } from "../../context";

export const teamMutations = {
  createTeam: async (
    _: any,
    args: { team: { name: string; description?: string } },
    ctx: IGraphQLContext
  ) => {
    return registerNewTeam({
      teamsRepo: ctx.repositories.teamsRepo,
      name: args.team.name,
      description: args.team.description,
    });
  },
};
