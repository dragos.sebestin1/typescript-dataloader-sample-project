import { listTeams } from "../../../domain/teams/teamCore";
import { IGraphQLContext } from "../../context";

export const teamQueries = {
  teams: async (_: any, args: any, ctx: IGraphQLContext) => {
    const result = await listTeams({
      teamRepo: ctx.repositories.teamsRepo,
      first: !!args.first ? parseInt(args.first) : undefined,
      after: !!args.after ? parseInt(args.after) : undefined,
    });

    return result;
  },
  team: async (_: any, args: { id: string }, ctx: IGraphQLContext) => ({
    id: args.id,
  }),
};

export const teamFieldResolvers = {
  Team: {
    id: async (root: { id: string }, _: any, ctx: IGraphQLContext) => {
      const team = await ctx.dataLoaders.teamDataLoader.load(parseInt(root.id));
      return team.id;
    },
    name: async (root: { id: string }, _: any, ctx: IGraphQLContext) => {
      const team = await ctx.dataLoaders.teamDataLoader.load(parseInt(root.id));
      return team.name;
    },
    description: async (root: { id: string }, _: any, ctx: IGraphQLContext) => {
      const team = await ctx.dataLoaders.teamDataLoader.load(parseInt(root.id));
      return team.description;
    },
    randomArg: () => {
      return Math.random();
    },
  },
};

export const teamEdgeResolvers = {
  TeamEdge: {
    node: teamFieldResolvers.Team,
  },
};
