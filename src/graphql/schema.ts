import * as path from "path";
import * as fs from "fs";
import { makeExecutableSchema } from "@graphql-tools/schema";
const GraphQLDate = require("graphql-date");

import { resolvers } from "./resolvers";

const gqlFiles = fs.readdirSync(path.join(__dirname, "./typedefs"));

const typeDefs = gqlFiles
  .map((filePath) =>
    fs.readFileSync(path.join(__dirname, "./typedefs", filePath), {
      encoding: "utf-8",
    })
  )
  .join("");

export const schema = makeExecutableSchema({
  typeDefs,
  resolvers: {
    Date: GraphQLDate,
    ...resolvers,
  },
});
