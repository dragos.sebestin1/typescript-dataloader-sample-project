import express from "express";
import { graphqlHTTP } from "express-graphql";
import { App } from "./app";
import { initGqlContext } from "./graphql/context";
import { schema } from "./graphql/schema";

// create an express server and GraphQL endpoint
const server = express();

/**
 * Start boostraping the app.
 */
async function bootstrap() {
  const app = new App();

  await app.start();

  server.use("/graphql", (req, res) =>
    graphqlHTTP({
      schema: schema,
      context: initGqlContext(app),
      graphiql: true,
    })(req, res)
  );

  // start listening
  const port = 5002;
  server.listen(port, () => {
    console.log(
      `GraphQL server with Express running on localhost:${port}/graphql`
    );
  });

  // handle process shutdown gracefully
  process.on("SIGINT", () => {
    app
      .end()
      .then(() => console.log("app closed succesfuly"))
      .catch((error) => console.error("app close failed", error));
  });
}

bootstrap()
  .then(() => console.log("app started succesfully"))
  .catch((error) => {
    console.error("failed to boostrap app", error);
    process.exit(1);
  });
