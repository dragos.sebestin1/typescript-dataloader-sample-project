import { expect } from "chai";
import { Campaign } from "../../domain/campaigns/campaign";
import { IBudget } from "../../domain/types";

describe("Campaign class", () => {
  describe("createNew", () => {
    it("should trim name", () => {
      const campaign = Campaign.createNew({
        team_id: 1,
        title: " Nexoya ",
        startDate: new Date(),
        endDate: new Date(),
      });

      expect(campaign.title).to.be.eq("Nexoya");
    });
  });

  describe("computeTotalBudget", () => {
    it("should correctly calculate the total budget", () => {
      const campaign = Campaign.createNew({
        team_id: 1,
        title: " Nexoya ",
        startDate: new Date(),
        endDate: new Date(),
      });

      const availableBudgets: IBudget[] = [
        {
          id: 1,
          campaignId: 1,
          providerId: 1,
          allocatedValue: 1,
        },
        {
          id: 1,
          campaignId: 1,
          providerId: 1,
          allocatedValue: 2,
        },
        {
          id: 1,
          campaignId: 1,
          providerId: 1,
          allocatedValue: 3,
        },
      ];

      const totalBudget = campaign.computeTotalBudget(availableBudgets);

      expect(totalBudget).to.be.eq(6);
    });
  });
});
